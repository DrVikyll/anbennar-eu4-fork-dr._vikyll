surakes_slot_2 = {
	slot = 2
	generic = no
	ai = yes
	potential = { tag = F52 }
	has_country_shield = yes

	#Expand the Local Administration
	surakes_civil_registry = { 
		icon = mission_city_prosperity
		required_missions = { surakes_surakes_made}
		position = 14
	
		provinces_to_highlight = { 
			and = {
				has_courthouse_building_trigger = no
				province_group = surakes_bulwari_cities 
			}
		}
	
		trigger = { 
			adm_power = 150
			surakes_bulwari_cities = {
				type = all
				has_courthouse_building_trigger = yes
			}
		}
	
		effect = { 
			add_adm_power = -150
			surakes_bulwari_cities = {
				add_permanent_province_modifier = {
					name = surakes_registry_modifier
					duration = -1
				}
			}
		}
	}
	
	#Professionalize the State
	surakes_centralize_education = { 
		icon = mission_writing_book
		required_missions = { surakes_civil_registry }
		position = 15
	
		provinces_to_highlight = { }
			
		trigger = { 
			or = {
				and = { 
					num_of_owned_provinces_with = {
						value = 5
						region = bahar_region
						or = {
							culture_group = bulwari
							culture_group = elven
							culture_group = dwarven
						}
						has_building = university							 
					}
					num_of_owned_provinces_with = {
						value = 5
						region = bulwar_proper_region
						or = {
							culture_group = bulwari
							culture_group = elven
							culture_group = dwarven
						}
						has_building = university							 
					}	
					num_of_owned_provinces_with = {
						value = 5
						region = harpy_hills_region
						or = {
							culture_group = bulwari
							culture_group = elven
							culture_group = dwarven
						}
						has_building = university							 
					}	
					num_of_owned_provinces_with = {
						value = 5
						region = far_bulwar_region
						or = {
							culture_group = bulwari
							culture_group = elven
							culture_group = dwarven
						}
						has_building = university							 
					}	
					num_of_owned_provinces_with = {
						value = 5
						region = far_salahad_region
						or = {
							culture_group = bulwari
							culture_group = elven
							culture_group = dwarven
						}
						has_building = university							 
					}	
				}
			has_active_policy = the_education_act
			}
		}
			
		effect = { country_event = { id = surakes_missions.2 } }
	}
	
	#The Golden Generation
	surakes_golden_generation = { 
		icon = mission_diplomacy_missive
		required_missions = { surakes_centralize_education }
		position = 17
	
		provinces_to_highlight = { }
			
		trigger = { 
			and = {	
				has_adm_advisor_3 = yes
				has_dip_advisor_3 = yes
				has_mil_advisor_3 = yes
			}
		}
		
		effect = { 
			add_country_modifier = {
				name = surakes_generation_modifier
				duration = 9125
			} 
		}
	}
	
	#Standard Surakesi
	surakes_academy_language = { 
		icon = mission_writing_book
		required_missions = { surakes_golden_generation }
		position = 18
	
		provinces_to_highlight = { }
			
		trigger = {
			stability = 3
			prestige = 75
			philosopher = 3
			statesman = 3
		}
		
		effect = { 
			add_country_modifier = {
				name = surakes_language_modifier
				duration = -1
			}
		}
	}
	
	#Daughters of Firanya
	surakes_harpy_hills = { 
		icon = mission_thinking_king
		required_missions = { surakes_remains_surakesi }
		position = 20
	
		provinces_to_highlight = {
			superregion = bulwar_superregion
			culture_group = harpy
			not = { owned_by = ROOT }
		}
				
		trigger = { 
			custom_trigger_tooltip = {
				tooltip = SURAKES_harpies_owned_tooltip
				not = {
					bulwar_superregion = {
						culture_group = harpy
						not = { owned_by = ROOT }
					}
				}
			}
					
			or = {
				custom_trigger_tooltip = {
					tooltip = SURAKES_has_harpies_tooltip
					num_of_owned_provinces_with = {
						value = 5
						superregion = bulwar_superregion
						or = {
							has_harpy_minority_trigger = yes
							has_harpy_majority_trigger = yes
						}
					}
				}
					
				custom_trigger_tooltip = {
					tooltip = SURAKES_not_harpies_tooltip
					 bulwar_superregion = {
                        type = all
                        has_harpy_minority_trigger = no
                        has_harpy_majority_trigger = no
                        owned_by = ROOT
                    }
				}
			}
		}
		
		effect = { 
			if = {
				limit = {  
					bulwar_superregion = {
						or = {
							has_harpy_minority_trigger = yes
							has_harpy_majority_trigger = yes
						}	
					}
				}
				country_event = { id = surakes_missions.5 }
			}
			else = { 
				add_prestige = 25
				add_adm_power= 200
			}
		}
	}

	#A Wall of Stone
	surakes_wall_stone = { 
		icon = mission_mountain_castle
		required_missions = { surakes_garden_surakes }
		position = 23
	
		provinces_to_highlight = { 
			and = {
				not = { has_building = fort_17th }
				province_group = surakes_bulwari_borders 
			}
		}
							
		trigger = { 
			surakes_bulwari_borders = {
				type = all
				has_building = fort_17th
			}
			or = {
				any_ally = {
					culture_group = dwarven
					has_opinion = {
						who = ROOT
						value = 150
					}
				}
				526 = { has_dwarven_hold_4 = yes }
			}
		}
			
		effect = { 
			surakes_bulwari_borders = {
				add_permanent_province_modifier = {
					name = surakes_stone_modifier
					duration = -1
				}
			}
		}		
	}
	
	#A Wall of Wood
	surakes_wall_wood = { 
		icon = mission_establish_high_seas_navy
		required_missions = { surakes_wall_stone }
		position = 24
	
		provinces_to_highlight = { }
			
		trigger = { 
			num_of_galley = 60
			num_of_heavy_ship = 30
		}
		
		effect = {	
			add_country_modifier = {
				name = surakes_armada_modifier
				duration = 9125
			}
		}	
	}
	
	#A Wall of Steel
	surakes_wall_steel = { 
		icon = mission_cannons_firing
		required_missions = { surakes_wall_wood }
		position = 25
	
		provinces_to_highlight = { }
			
		trigger = { army_size = 150 }
		
		effect = { 
			add_country_modifier = {
				name = surakes_sunforged_modifier
				duration = 9125
			}
		}
	}	
}

surakes_slot_3 = {
	slot = 3
	generic = no
	ai = yes
	potential = { tag = F52 }
	has_country_shield = yes

	#Surakes has Been Made
	surakes_surakes_made = { 
		icon = mission_sunset
		required_missions = { }
		position = 13
	
		provinces_to_highlight = { 
			and = {
				superregion = bulwar_superregion
				culture_group = bulwari
				not = { owned_by = ROOT }
			}
		}
	
		trigger = {
			custom_trigger_tooltip = {
				tooltip = SURAKES_own_bulwar_tooltip
				not = {
					bulwar_superregion = {
						culture_group = bulwari
						not = { owned_by = ROOT }
					}
				}
			}
		}
		
	
		effect = { country_event = { id = surakes_missions.1 } }
	}

	#Looking for Copper
	surakes_looking_cooper = { 
		icon = mission_monarch_diplomacy
		required_missions = { surakes_surakes_made }
		position = 15
	
		provinces_to_highlight = { province_id = 526 }
			
		trigger = { 
			accepted_culture = copper_dwarf
			526 = {
				is_core = yes
				has_dwarven_hold_3 = yes
			}
		}
		
		effect = { 
			526 = {
				add_province_modifier = {
					name = surakes_hold_modifier
					duration = 9125
				}
			}
			capital_scope = {
				add_dwarven_minority_size_effect = yes
			}
		}
	}
	
	#I Found the Sun
	surakes_found_sun = { 
		icon = mission_monarch_diplomacy
		required_missions = { surakes_looking_cooper }
		position = 16
	
		provinces_to_highlight = {
			and = {
				owned_by = ROOT
				culture = sun_elf 
			}
		}
			
		trigger = { 
			accepted_culture = sun_elf
			num_of_owned_provinces_with = {
				value = 5
				culture = sun_elf
				development = 20
				has_tax_building_trigger = yes
				has_trade_building_trigger = yes
			}
		}
	
		effect = {
				if = {
					limit = { culture = sun_elf }
					add_province_modifier = {
						name = surakes_elvencity_modifier
						duration = 9125
					}
				}
			capital_scope = {
				add_elven_minority_size_effect = yes
			}
		}
	}
	
	#An Anthem for Surakes
	surakes_anthem_surakes = { 
		icon = mission_diplomacy_missive
		required_missions = { surakes_golden_generation }
		position = 18
	
		provinces_to_highlight = { }
			
		trigger = { 
			advisor = artist
			or = {
				or = { 	
					army_tradition = 50
					navy_tradition = 50
				}
				full_idea_group = quantity_ideas
			}
		}
		
		effect = { country_event = { id = surakes_missions.3 } }
	}
	
	#Now it Remains to Make Surakesi
	surakes_remains_surakesi = { 
		icon = mission_sunset
		required_missions = { 	
			surakes_academy_language
			surakes_anthem_surakes
			surakes_solstice_festival 
		}
		position = 19
	
		provinces_to_highlight = { }
			
		trigger = {
			stability = 3
			if = { 
				limit = { government = kingdom }
				absolutism = 80
			}
			else = { absolutism = 40 }
			or = { 	
				and = {
					legitimacy_equivalent = 100
					num_accepted_cultures = 3
					estate_loyalty = {
						estate = estate_burghers
						loyalty = 60
					}
					religious_unity = 1
				}
					
				and = {
					or = {
						has_active_policy = cultural_unity
						has_active_policy = cultural_recognition_act
					}
					religious_unity = 1
				}
			}
		}
			
		effect = { 
			every_owned_province = {
				limit = { 
					or = {
						culture_group = bulwari
						culture_group = dwarven
						culture_group = elven
					} 
				}
				add_province_modifier = {
					name = surakes_nationalism_modifier
					duration = -1
				}
			}
		}
	}
	
	#Deliver Us
	surakes_deliver_us = { 
		icon = mission_thinking_king
		required_missions = { surakes_remains_surakesi }
		position = 20
	
		provinces_to_highlight = { 
			and = {
				superregion = bulwar_superregion
				culture_group = goblin
				not = { owned_by = ROOT }
			}
		}
			
		trigger = { 
			custom_trigger_tooltip = {
				tooltip = SURAKES_goblin_owned_tooltip
				not = {
					bulwar_superregion = {
						culture_group = goblin
						not = { owned_by = ROOT }
					}
				}
			}	
			or = {
				custom_trigger_tooltip = {
					tooltip = SURAKES_has_goblins_tooltip
					num_of_owned_provinces_with = {
						value = 3
						superregion = bulwar_superregion
						or = {
							has_goblin_minority_trigger = yes
							has_goblin_majority_trigger = yes
						}
					}
				}
				custom_trigger_tooltip = {
					tooltip = SURAKES_not_goblins_tooltip
					 bulwar_superregion = {
                        type = all
                        has_goblin_minority_trigger = no
                        has_goblin_majority_trigger = no
                        owned_by = ROOT
                    }
				}
			}
		}
		
		effect = { 
			if = {
				limit = {  
					bulwar_superregion = {
						or = {
							has_goblin_minority_trigger = yes
							has_goblin_majority_trigger = yes
						}	
					}
				}
				country_event = { id = surakes_missions.6 }
			}
			else = { 
				add_prestige = 25
				add_adm_power= 200
			}
		}
	}
	
	#What is a Bulwari?
	surakes_whats_bulwari = { 
		icon = mission_sunset
		required_missions = { 	
			surakes_harpy_hills
			surakes_deliver_us
			surakes_gnoll_question
		}
		position = 21
	
		provinces_to_highlight = { }
			
		trigger = { adm_power = 250 }
		
		effect = { 
			add_adm_power = -250
			country_event = { id = surakes_missions.8 }
		}
	}
	
	#The Garden of Surakes
	surakes_garden_surakes = { 
		icon = mission_sunset
		required_missions = { surakes_whats_bulwari }
		position = 22
	
		provinces_to_highlight = { superregion = bulwar_superregion }
			
		trigger = { 
			bulwar_superregion = {
				type = all
				owned_by = ROOT
			}
			adm_power = 150
			dip_power = 150
			treasury = 5000
		}
		
		effect = { 
			add_adm_power = -150
			add_dip_power = -150
			add_treasury = -5000
			country_event = { id = surakes_missions.9 }
		}
	}
	
	#Gardens in the River
	surakes_flowers_river = { 
		icon = mission_desert_city
		required_missions = { surakes_garden_surakes }
		position = 23
		
		provinces_to_highlight = { province_group = surakes_river_provinces }
			
		trigger = { 
			surakes_river_provinces = {
				type = all
				development = 25
				devastation = 0
				has_tax_building_trigger = yes
			}
		}
		
		effect = { 	
			add_prestige = 20
			country_event = { id = surakes_missions.10 days = 1825 }  
		}
	}
	
	#Gardens in the Desert
	surakes_flowers_desert = { 
		icon = mission_desert_city
		required_missions = { surakes_flowers_river }
		position = 24
	
		provinces_to_highlight = { province_group = surakes_desert_provinces  }
			
		trigger = { 
			surakes_desert_provinces = {
				type = all
				development = 18
				devastation = 0
				has_tax_building_trigger = yes
			}
		}
		
		effect = { 	
			add_prestige = 20
			country_event = { id = surakes_missions.12 days = 1825  } 
		}
	}
	
	#Gardens in the Mountains
	surakes_flowers_mountains = { 
		icon = mission_desert_city
		required_missions = { surakes_flowers_desert }
		position = 25
	
		provinces_to_highlight = { province_group = surakes_mountain_provinces }
			
		trigger = { 
			surakes_mountain_provinces = {
				type = all
				development = 18
				devastation = 0
				has_tax_building_trigger = yes
			}
		}
		
		effect = { 	
			add_prestige = 20
			country_event = { id = surakes_missions.11 days = 1825  } 
		}
	}
	
	#The Earthly Paradise
	surakes_earthly_paradise = { 
		icon = mission_green_village
		required_missions = { 	
			surakes_wall_steel
			surakes_flowers_mountains
			surakes_golden_bazaar
		}
		position = 26
	
		provinces_to_highlight = { province_id = 601 }
			
		trigger = { 
			601 = { 
				owned_by = ROOT
				has_province_modifier = bulwari_locks_and_lifts
			}
			treasury = 5000
			adm_power = 200
		}
		
		effect = { 	
			add_treasury = -5000
			add_adm_power = -200
			country_event = { id = surakes_missions.13 days = 3650 }
		}
	}
}

surakes_slot_4 = {
	slot = 4
	generic = no
	ai = yes
	potential = { tag = F52 }
	
	#Desacralize the Cult
	surakes_nationalize_cult = { 
		icon = mission_sun_temple
		required_missions = { surakes_surakes_made }
		position = 14
	
		provinces_to_highlight = { }
			
		trigger = {
			if = { 
				limit = { religion = bulwari_sun_cult }
				bulwar_superregion = {
					and = {
						not = { primary_culture = sun_elf }
						not = { religion = bulwari_sun_cult }
					}
				}
			}
			else_if = { limit = { religion = old_bulwari_sun_cult }
				religious_unity = 1 #Temporal - Own all holy places
			}
			else_if = { 
				limit = { religion = the_jadd }
				not = { exists = F46 }
				not = { exists = F51 }
			}
			absolutism = 30
		}		
						
		effect = {
			custom_tooltip = SURAKES_desacralize_tt
			hidden_effect = {
				every_owned_province = {
					limit = { religion_group = bulwari }
					add_province_modifier = {
						name = surakes_nationalized_modifier
						duration = 3650
					}
				}
			}
			add_country_modifier = {
				name = surakes_nationalcult_modifier
				duration = 3650
			}
		}
	}
	
	#One Sun over Surakes
	surakes_one_sun = { 
		icon = mission_sun_temple
		required_missions = { surakes_nationalize_cult }
		position = 15
	
		provinces_to_highlight = { }
			
		trigger = { religious_unity = 1 }
		
		effect = { 
			add_country_modifier = {
				name = surakes_onesun_modifier
				duration = 18250
			}
		}
	}
	
	#Lessons from our Past
	surakes_lessons_past = { 
		icon = mission_desert_pilgrim
		required_missions = { surakes_one_sun }
		position = 16
	
		provinces_to_highlight = {
			province_id = 601
			province_id = 565
			province_id = 631
			province_id = 591
		}
			
		trigger = { 
			and = {
				601 = { infantry_in_province = 5 }
				565 = { infantry_in_province = 5 }
				631 = { infantry_in_province = 5 }
				591 = { infantry_in_province = 5 }
			}
		}
		
		effect = { country_event = { id = surakes_missions.14 } }
	}
	
	#We Need a Hero
	surakes_need_hero = { 
		icon = mission_persian_soldiers
		required_missions = { surakes_lessons_past }
		position = 17
	
		provinces_to_highlight = { }
			
		trigger = { 
			adm_power = 200
			dip_power = 200
			mil_power = 200 
		}
		
		effect = { 
			add_adm_power = -200
			add_dip_power = -200
			add_mil_power = -200
			country_event = { id = surakes_missions.4 } 
		}
	}
	
	#The Solstice Festival
	surakes_solstice_festival = { 
		icon = mission_sunset
		required_missions = { surakes_need_hero }
		position = 18
	
		provinces_to_highlight = { }
			
		trigger = { 
			treasury = 2500
			overextension_percentage = 0
			num_of_owned_provinces_with = {
				value = 20
				has_tax_building_trigger = yes
			}
		}
		
		effect = { 
			add_treasury = -2500
			country_event = {id = surakes_missions.16 } 
		}
	}
	
	#The Gnoll Question
	surakes_gnoll_question = { 
		icon = mission_thinking_king
		required_missions = { surakes_remains_surakesi }
		position = 20
	
		provinces_to_highlight = {  
				and = {
					superregion = bulwar_superregion
					culture_group = gnollish
					not = { owned_by = ROOT }
				}
			}
			
		trigger = { 
			custom_trigger_tooltip = {
				tooltip = SURAKES_gnoll_owned_tooltip
				not = {
					bulwar_superregion = {
						culture_group = gnollish
						not = { owned_by = ROOT }
					}
				}
			}
					
			or = {
				custom_trigger_tooltip = {
					tooltip = SURAKES_has_gnolls_tooltip
					num_of_owned_provinces_with = {
						value = 5
						superregion = bulwar_superregion
						or = {
							has_gnollish_minority_trigger = yes
							has_gnollish_majority_trigger = yes
						}
					}
				}
				custom_trigger_tooltip = {
					tooltip = SURAKES_not_gnolls_tooltip
					 bulwar_superregion = {
                        type = all
                        has_gnollish_minority_trigger = no
                        has_gnollish_majority_trigger = no
                        owned_by = ROOT
                    }
				}
			}
		}
		
		effect = { 
			if = {
				limit = {  
					bulwar_superregion = {
						or = {
							has_gnollish_minority_trigger = yes
							has_gnollish_majority_trigger = yes
						}	
					}
				}
				country_event = { id = surakes_missions.7 }
			}
			else = { 
				add_prestige = 25
				add_adm_power= 200
			}
		}
	}
	
	#A Golden Road
	surakes_golden_road = { 
		icon = mission_merchant_trip
		required_missions = { surakes_garden_surakes }
		position = 23
	
		provinces_to_highlight = { 
			or = {
				province_group = golden_highway_bulwar_proper
				province_group = golden_highway_far_bulwar
				province_group = golden_highway_far_salahad
			}
			not = {
				has_province_modifier = golden_highway
			}
		}
			
		trigger = { 
			golden_highway_bulwar_proper = {
				type = all
				has_province_modifier = golden_highway
			}
			golden_highway_far_bulwar = {
				type = all
				has_province_modifier = golden_highway
			}
			golden_highway_far_salahad = {
				type = all
				has_province_modifier = golden_highway
			}
		}
	
		effect = { 
			add_country_modifier = {
				name = surakes_road_modifier
				duration = 7300
			}
		}
	}
	
	#A Golden Sea Lane
	surakes_golden_sea = { 
		icon = mission_galleys_in_port
		required_missions = { surakes_golden_road }
		position = 24
	
		provinces_to_highlight = { }
			
		trigger = { 
			 num_of_light_ship = 100
			526 = {
				trade_share = {
					country = ROOT
					share = 40
				}
			}
			451 = {
				trade_share = {
					country = ROOT
					share = 40
				}
			}
			380 = {
				trade_share = {
					country = ROOT
					share = 40
				}
			}
			565 = {
				trade_share = {
					country = ROOT
					share = 40
				}
			}
			475 = {
				trade_share = {
					country = ROOT
					share = 40
				}
			}
		}
		
		effect = { 
			add_country_modifier = {
			   name = surakes_lane_modifier
			   duration = 7300
			}
		}
	}
	
	#A Golden Bazaar
	surakes_golden_bazaar = { 
		icon = mission_market_place_with_asian_traders
		required_missions = { surakes_golden_sea }
		position = 25
		
		provinces_to_highlight = { province_id = 565 }
			
		trigger = { 
			565 = {
				development = 50
				province_has_center_of_trade_of_level = 3
				num_free_building_slots = 0
				has_trade_building_trigger = yes
			}
		}
		
		effect = { 
			565 = {
				add_province_modifier = {
					name = surakes_bazaar_modifier
					duration = -1
				}
			}	
		}
	}
}