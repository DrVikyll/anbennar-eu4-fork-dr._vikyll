theocracy_mechanic = {
	has_devotion = yes
	rulers_can_be_generals = no
	heirs_can_be_generals = no
	royal_marriage = no
	different_religion_acceptance = -20
	different_religion_group_acceptance = -50
	religion = yes
	heir = yes
	basic_reform = yes # = invisible/does not take up a slot
	valid_for_nation_designer = no

	modifiers = {
		brahmins_hindu_influence_modifier = 0.1
		church_loyalty_modifier = 0.1
		church_influence_modifier = 0.05
	}
}

#Leadership
leading_clergy_reform = {
	potential = {
		NOT = { tag = PAP }
		NOT = { tag = Z97 }	#Ravelian State
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	allow_normal_conversion = yes
	legacy_equivalent = theocratic_government
	icon = "religious_leader"
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	modifiers = {
		global_tax_modifier = 0.05
	}
}

monastic_order_reform = {
	potential = {
		NOT = { tag = PAP }
		NOT = { tag = Z97 }
		is_temple = no	#Anbennar
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	modifiers = {
		fort_maintenance_modifier = -0.2
	}
	rulers_can_be_generals = yes
	heirs_can_be_generals = yes
	valid_for_nation_designer = yes
	nation_designer_cost = 10
	icon = "monks"
	allow_normal_conversion = yes
	legacy_equivalent = monastic_order_government
	monastic = yes
	fixed_rank = 1
}

#Anbennar - changed this to High Rectorate
papacy_reform = {
	potential = {
		tag = Z97
		NOT = { has_adventurer_reform = yes } # Anbennar
	}

	modifiers = {
	
		#Anbennar
		diplomatic_upkeep = 1
		embracement_cost = -0.25
		church_influence_modifier = 0.1
		artificers_influence_modifier = 0.1

	}
	valid_for_nation_designer = no
	icon = "papacy"
	allow_normal_conversion = yes
	legacy_equivalent = papal_government
	papacy = yes
	allow_convert = no
	lock_level_when_selected = yes
	fixed_rank = 2

	custom_attributes = {
		locked_government_type = yes
	}
}

# Anbennar
wulin = {
	icon = "shogunate"
	allow_normal_conversion = yes
	potential = {
		NOT = { has_country_flag = formed_one_xia_flag }
		# OR = {
		# 	AND = {
		# 		capital_scope = {
		# 			province_id = 4808
		# 		}
		# 		#culture_group = japanese_g
		# 		NOT = { exists = JAP }
		# 	}
		# 	AND = {
		# 		is_playing_custom_nation = yes
		# 		have_had_reform = wulin
		# 	}
		# }
		religion = righteous_path
		OR = {
			AND = {
				capital_scope = {
					province_id = 4559
				}
				has_reform = wulin
			}
			AND = {
				is_playing_custom_nation = yes
				have_had_reform = wulin
			}
		}
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	valid_for_nation_designer = yes
	nation_designer_cost = 50
	nation_designer_trigger = {
		#culture_group = japanese_g
		#technology_group = chinese
		capital_scope = { continent = asia }
		#owns = 4808
		owns = 4808
	}
	legacy_equivalent = shogunate_legacy
	fixed_rank = 2
	#maintain_dynasty = yes
	allow_vassal_war = yes
	allow_vassal_alliance = yes
	lock_level_when_selected = yes

	modifiers = {
		diplomats = 1
		free_leader_pool = 1
		governing_capacity = -100
	}

	custom_attributes = {
		locked_government_type = yes
	}
}

xiaken = {
	icon = "daimyo"
	allow_normal_conversion = no
	potential = {
		religion = righteous_path
		NOT = { has_country_flag = formed_one_xia_flag }
		overlord = { has_reform = wulin }
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	legacy_equivalent = daimyo_legacy
	fixed_rank = 1
	#maintain_dynasty = yes
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	lock_level_when_selected = yes
	nation_designer_trigger = {
		#culture_group = japanese_g
		#technology_group = chinese
		capital_scope = { continent = asia }
	}
	modifiers = {
		land_morale = 0.1
		infantry_power = 0.1
		governing_capacity = -100
	}
	custom_attributes = {
		locked_government_type = yes
	}
}

indep_xiaken = {
	icon = "samurai"
	allow_normal_conversion = no
	potential = {
		religion = righteous_path
		NOT = { has_country_flag = formed_one_xia_flag }
		OR = {
			AND = {
				OR = {
					is_subject = no
					overlord = { NOT = { has_reform = wulin } }
				}
				#capital_scope = { region = japan_region }
				#culture_group = japanese_g
			}
			AND = {
				is_playing_custom_nation = yes
				have_had_reform = indep_xiaken
			}
		}
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	nation_designer_trigger = {
		#culture_group = japanese_g
		#technology_group = chinese
		capital_scope = { continent = asia }
	}
	legacy_equivalent = indep_daimyo_legacy
	#maintain_dynasty = yes

	modifiers = {
		land_morale = 0.1
		infantry_power = 0.1
		governing_capacity = -150
	}
	custom_attributes = {
		locked_government_type = yes
	}
}

secular_order_reform = {
	potential = {
		is_temple = no
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	
	modifiers = {
		fort_maintenance_modifier = -0.2
	}
	valid_for_nation_designer = yes
	nation_designer_cost = 10
	icon = "soldiers"
	allow_normal_conversion = no
	legacy_equivalent = secular_order_government
	monastic = yes
	fixed_rank = 1
	
	different_religion_acceptance = 0
	different_religion_group_acceptance = 0
	rulers_can_be_generals = yes
	#allow_migration = yes
}

adventurer_reform = {
	potential = {
		has_adventurer_reform = yes	#aka you cant see it unless you're already one
		OR = {
			is_adventurer = yes
			is_playing_custom_nation = yes
		}
	}
	modifiers = {
		manpower_recovery_speed = 0.33
		#colonists = 1
		diplomats = -1
		#diplomatic_upkeep = -2
		#reform_progress_growth = -0.75
		#land_forcelimit = 3

		#New Ones
		tribal_development_growth = 0.02
		#institution_spread_from_true_faith = 3.0 # Very big but otherwise there's never any institutions, remove this once a better solution is found
		
		#Taken from natives
		province_warscore_cost = 5 # Cause players get some for free now, this actually has a really tiny impact lol
		reform_progress_growth = -0.5
		monthly_reform_progress = 0.75
	}
	valid_for_nation_designer = yes
	nation_designer_cost = 10
	icon = "soldiers_2"
	allow_normal_conversion = no
	lock_level_when_selected = yes
	fixed_rank = 1
	#republican_name = yes
	legacy_equivalent = adventurer
	monastic = yes
	
	different_religion_acceptance = 0
	different_religion_group_acceptance = 0
	rulers_can_be_generals = yes
	allow_migration = yes
	native_mechanic = yes
	
	factions = {
		adv_marchers
		adv_pioneers
		adv_fortune_seekers
	}
	
	conditional = {
		allow = { has_dlc = "Rights of Man" }
		militarised_society = yes
	}
	
	custom_attributes = {
		locked_government_type = yes
	}
}

dwarovar_adventurer_reform = {
	potential = {
		has_reform = dwarovar_adventurer_reform	#aka you cant see it unless you're already one
		OR = {
			is_adventurer = yes
			is_playing_custom_nation = yes
		}
	}
	modifiers = {
		manpower_recovery_speed = 0.33
		#colonists = 1
		diplomats = -1
		#diplomatic_upkeep = -2
		reform_progress_growth = -0.75
		#land_forcelimit = 3
		
	}
	valid_for_nation_designer = yes
	nation_designer_cost = 10
	icon = "soldiers_2"
	allow_normal_conversion = no
	lock_level_when_selected = yes
	fixed_rank = 1
	#republican_name = yes
	legacy_equivalent = adventurer
	monastic = yes
	
	different_religion_acceptance = 0
	different_religion_group_acceptance = 0
	rulers_can_be_generals = yes
	native_mechanic = no
	allow_migration = yes
	
	factions = {
		adv_marchers
		adv_pioneers
		adv_fortune_seekers
	}
	
	conditional = {
		allow = { has_dlc = "Rights of Man" }
		militarised_society = yes
	}
	
	custom_attributes = {
		ai_migration = yes
		locked_government_type = yes
	}
}

aelantir_adventurer_reform = {
	potential = {
		has_reform = aelantir_adventurer_reform	#aka you cant see it unless you're already one
		OR = {
			is_adventurer = yes
			is_playing_custom_nation = yes
		}
	}
	modifiers = {
		manpower_recovery_speed = 0.33
		colonists = 1	#so they can colonize?
		diplomats = -1
		#diplomatic_upkeep = -2
		reform_progress_growth = -0.75
		#land_forcelimit = 3
		
	}
	valid_for_nation_designer = yes
	nation_designer_cost = 10
	icon = "soldiers_2"
	allow_normal_conversion = no
	lock_level_when_selected = yes
	fixed_rank = 1
	#republican_name = yes
	legacy_equivalent = adventurer
	monastic = yes
	
	different_religion_acceptance = 0
	different_religion_group_acceptance = 0
	rulers_can_be_generals = yes
	native_mechanic = no
	allow_migration = yes
	
	factions = {
		adv_marchers
		adv_pioneers
		adv_fortune_seekers
	}
	
	conditional = {
		allow = { has_dlc = "Rights of Man" }
		militarised_society = yes
	}
	
	custom_attributes = {
		ai_migration = yes
		locked_government_type = yes
		enable_tribal_grazing = yes
	}
}

magisterium_reform = {
	potential = {
		tag = A85
	}

	modifiers = {	#for better spellcasting
		monarch_admin_power = 1
		monarch_diplomatic_power = 1
		monarch_military_power = 1
		
		mages_loyalty_modifier = 0.1
		mages_influence_modifier = 0.1
	}
	valid_for_nation_designer = no
	icon = "papacy"
	allow_normal_conversion = no
	legacy_equivalent = magisterium
	lock_level_when_selected = yes
	fixed_rank = 2
	
	rulers_can_be_generals = yes
	heirs_can_be_generals = yes

	custom_attributes = {
		locked_government_type = yes
		is_magocracy = yes
	}

	effect = {
		#count_gov_as_magocracy = yes #deprecated
	}

	removed_effect = {
		#reset_gov_is_magocracy = yes #deprecated
	}
}

magocracy_reform = {
	potential = {
		OR = {
			has_estate = estate_mages
			has_country_flag = adventurer_derived_government
			}
		OR = {
			estate_influence = {
				estate = estate_mages
				influence = 20
			}
			has_reform = magocracy_reform
			culture_group = eordellon_ruinborn_elf
			has_country_flag = adventurer_derived_government
		}
		is_temple = no
	}

	modifiers = {	#for better spellcasting
		all_power_cost = -0.05
		
		mages_influence_modifier = 0.1
	}
	custom_attributes = {
		is_magocracy = yes
	}
	valid_for_nation_designer = yes
	nation_designer_cost = 20
	icon = "papacy"
	allow_normal_conversion = no
	legacy_equivalent = magocracy
	lock_level_when_selected = yes
	
	rulers_can_be_generals = yes
	heirs_can_be_generals = yes
}

rezankand_reform = { #Rezankand government
	icon = "papacy"
	allow_normal_conversion = no
	potential = {
		OR = {
			AND = { 
				has_country_flag = rezankand_government
				OR = {
					tag = H67
				}
			}
			is_playing_custom_nation = yes
		}
	}
	
	valid_for_nation_designer = yes
	nation_designer_cost = 10
	nation_designer_trigger = {
	
	}
	
	legacy_equivalent = rezankand_reform_legacy
	lock_level_when_selected = yes
	fixed_rank = 3
	maintain_dynasty = yes

	rulers_can_be_generals = yes
	heirs_can_be_generals = yes
	
	modifiers = {
		max_absolutism = 10
		institution_spread_from_true_faith = 0.15
		administrative_efficiency = 0.05
		num_accepted_cultures = 2
	}
	
	custom_attributes = {
		locked_government_type = yes
	}
}

oracular_order_reform = {
	potential = {
		OR = { 
			tag = R05
			have_had_reform = oracular_order_reform
		}
	}

	modifiers = {
		prestige_decay = -0.02
		all_power_cost = -0.1
		core_creation = 1
		enemy_core_creation = 1
		governing_capacity = -150
	}
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	icon = "papacy"
	allow_normal_conversion = no
	legacy_equivalent = theocratic_government
	lock_level_when_selected = yes
	fixed_rank = 1
	
	rulers_can_be_generals = no
	heirs_can_be_generals = yes

	custom_attributes = {
		locked_government_type = yes
	}
}

gerudia_skaldhyrric_order = {
	potential = {
		NOT = { has_adventurer_reform = yes } # Anbennar
		religion = skaldhyrric_faith
	}
	modifiers = {
		advisor_pool = 1
		improve_relation_modifier = 0.1
	}
	valid_for_nation_designer = yes
	nation_designer_cost = 0
	nation_designer_trigger = {
		religion = skaldhyrric_faith
	}
	icon = "man_on_podium"
	allow_normal_conversion = no
	legacy_equivalent = theocratic_government
	lock_level_when_selected = no
	rulers_can_be_generals = no
	heirs_can_be_generals = yes
}

ancestor_venerator_reform = {
	potential = {
		OR = { 
			has_reform = ancestor_venerator_reform
			have_had_reform = ancestor_venerator_reform
		}
	}

	modifiers = {
		tolerance_own = 2
		tolerance_heathen = -3
		adventurers_loyalty_modifier = 0.1
	}
	valid_for_nation_designer = yes
	nation_designer_cost = 10
	icon = "sacred_elven_empire"
	allow_normal_conversion = no
	legacy_equivalent = theocratic_government
	lock_level_when_selected = yes
	
	rulers_can_be_generals = no
	heirs_can_be_generals = yes

	custom_attributes = {
		locked_government_type = yes
	}
}

#Internal vs External Mission

internal_mission_reform = {
	icon = "clergyman"
	allow_normal_conversion = yes
	potential = {
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	modifiers = {
		tolerance_own = 2
	}
}

external_mission_reform = {
	icon = "soldiers"
	allow_normal_conversion = yes
	potential = {
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	modifiers = {
		manpower_recovery_speed = 0.1
	}
}

mission_to_civilize_reform = {
	icon = "mission_to_civilize_reform"
	allow_normal_conversion = yes
	potential = {
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	trigger = {
		OR = {
			num_of_colonists = 1
			has_idea_group = exploration_ideas
			has_idea_group = expansion_ideas
		}
	}
	modifiers = {
		native_assimilation = 0.35
		native_uprising_chance = -0.5
	}
}

mission_to_kill_pirates_reform = {
	icon = "mission_to_kill_pirates_reform"
	allow_normal_conversion = yes
	potential = {
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	potential = {
		capital_scope = {
			has_port = yes
		}
	}
	trigger = {
		capital_scope = {
			has_port = yes
		}
	}
	modifiers = {
		global_sailors_modifier = 0.2
		naval_forcelimit_modifier = 0.25
		capture_ship_chance = 0.33
	}
}

#Internal vs External Mission - Anbennar

exploring_the_frontiers = {
	icon = "soldiers_4"
	potential = {
		has_reform = adventurer_reform
		OR = {
			is_adventurer = yes
			is_playing_custom_nation = yes
		}
	}
	modifiers = {
		migration_cost = -0.15
	}
}

growing_the_band = {
	icon = "paper_money_map"
	potential = {
		has_reform = adventurer_reform
		OR = {
			is_adventurer = yes
			is_playing_custom_nation = yes
		}
	}
	modifiers = {
		tribal_development_growth = 0.04
	}
	
	ai = {
		factor = 0.5

		modifier = { 
			factor = 2
			NOT = { tribal_development = 10 } # We are somehow still quite small
		}
	}
}

the_glorious_quest = {
	icon = "tribal_centralize_power"
	potential = {
		has_reform = adventurer_reform
		OR = {
			is_adventurer = yes
			is_playing_custom_nation = yes
		}
	}
	modifiers = {
		reform_progress_growth = 0.25
		prestige = 0.5
	}
}


serpentspine_spelunking = {
	icon = "soldiers_4"
	potential = {
		has_reform = dwarovar_adventurer_reform
	}
	modifiers = {
		prestige = 0.5
		movement_speed = 0.1
	}
}

call_to_the_surfacers = {
	icon = "soldiers_4"
	potential = {
		has_reform = dwarovar_adventurer_reform
	}
	modifiers = {
		global_colonial_growth = 15
	}
}

treasures_of_our_forefathers = {
	icon = "soldiers_4"
	potential = {
		has_reform = dwarovar_adventurer_reform
	}
	modifiers = {
		monthly_militarized_society = 0.1
	}
}

#Divine Cause

safeguard_holy_sites_reform = {
	icon = "church"
	allow_normal_conversion = yes
	potential = {
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	modifiers = {
		prestige = 1
	}
}

combat_heresy_reform = {
	icon = "soldiers_2"
	allow_normal_conversion = yes
	potential = {
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	modifiers = {
		land_morale = 0.1
	}
}

expel_heathens_reform = {
	icon = "landscape"
	allow_normal_conversion = yes
	potential = {
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	modifiers = {
		development_cost = -0.05
	}
}

kingdom_of_god_reform = {
	icon = "kingdom_of_god"
	potential = {
		has_reform = papacy_reform
		OR = {
			has_reform = kingdom_of_god_reform
			have_had_reform = kingdom_of_god_reform
		}
	}
	modifiers = {
		global_manpower_modifier = 0.1
		prestige = 1
		devotion = 1
		appoint_cardinal_cost = -0.3
		papal_influence_from_cardinals = 1.0
	}
	allow_normal_conversion = yes
	lock_level_when_selected = yes
	fixed_rank = 3
}

#Divine Cause - Anbennar
gerudia_keep_dragon_aslumber = {
	icon = "soldiers_4"
	potential = {
		religion = skaldhyrric_faith
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	allow_normal_conversion = yes
	modifiers = {
		defensiveness = 0.1
		enemy_core_creation = 0.2
	}
}


stake_our_claim = {
	icon = "dutch_flag"
	potential = {
		has_reform = adventurer_reform
		OR = {
			is_adventurer = yes
			is_playing_custom_nation = yes
		}
	}
	trigger = {
		if = {
			limit = { ai = no has_dlc = "Conquest of Paradise" NOT = { has_reform = stake_our_claim } } # Fuck you players
			custom_trigger_tooltip = {
				tooltip = "MUST_BE_IN_TRIBAL_LAND_TO_SETTLE"
				capital_scope = { tribal_land_of = ROOT }
			}
		}
	}
	modifiers = {
		add_tribal_land_cost = 1
		province_warscore_cost = 0.75
		stability_cost_modifier = -0.1
	}
	conditional = {
		allow = { has_dlc = "Conquest of Paradise" }
		allow_migration = no
		custom_attributes = {
			enable_settled_mechanics = yes
			enable_tribal_grazing = no
		}
	}
	effect = {
		custom_tooltip = claim_all_adjacent_monster_tt
		hidden_effect = {
			capital_scope = {
				tribal_owner = {
					if = {
						limit = { ai = yes }
						country_event = { id = adventurer.4 } # Don't sent yourself the event if you're not an AI
					}
				}
				change_tribal_land = ROOT
				# Take monsters tribal land
				every_empty_neighbor_province = {
					limit = { 
						tribal_owner = { 
							is_greentide_horde = yes  # For some reason checking for the modifier doesn't work
						} 
					}
					tribal_owner = {
						country_event = { id = adventurer.4 }
					}
					change_tribal_land = ROOT
					if = {
						limit = { ROOT = { ai = no } }
						ROOT = {
							add_adm_power = -50
							add_treasury = -100
						}
					}
				}
				# Take monster tribal land
				every_empty_neighbor_province = {
						# Don't remove the NOT and change to an exists = no. You CANNOT scope to a tag that does not exist so this is the only way this works.
					limit = {
						NOT = {
							tribal_owner = { 
								exists = yes
							}
						}
					}
					change_tribal_land = ROOT
					if = {
						limit = { ROOT = { ai = no } }
						add_adm_power = -50
						add_treasury = -100
					}
				}
			}
		}
	}
	lock_level_when_selected = yes

	ai = {
		factor = 1
	}
}

end_the_greentide = {
	icon = "fist_in_air"
	potential = {
		has_reform = adventurer_reform
		OR = {
			is_adventurer = yes
			is_playing_custom_nation = yes
		}
	}
	modifiers = {
		migration_cost = -0.25
		warscore_cost_vs_other_religion = -1.0
		add_tribal_land_cost = -0.5
	}
	ai = {
		factor = 0.75
	}
}


righting_the_oldest_grudge = {
	icon = "soldiers_4"
	potential = {
		has_reform = dwarovar_adventurer_reform
	}
	modifiers = {
		manpower_recovery_speed = 0.10
		warscore_cost_vs_other_religion = -0.5
	}
}


hold_restoration = {
	icon = "soldiers_4"
	potential = {
		has_reform = dwarovar_adventurer_reform
	}
	modifiers = {
		build_time = -0.05
		global_unrest = -1
	}
}

#Clergy in Administration

subservient_administrators_reform = {
	icon = "paper_with_seal"
	allow_normal_conversion = yes
	potential = {
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	modifiers = {
		yearly_corruption = -0.1
	}
}

religious_administrators_reform = {
	icon = "paper_with_seal_3"
	allow_normal_conversion = yes
	potential = {
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	modifiers = {
		stability_cost_modifier = -0.1
	}
}

pious_merchants_reform = {
	icon = "merchant"
	allow_normal_conversion = yes
	potential = {
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	modifiers = {
		global_trade_power = 0.05
		maratha_muslim_influence_modifier = 0.1
		rajput_muslim_influence_modifier = 0.1
		burghers_influence_modifier = 0.1
		burghers_loyalty_modifier = 0.05
		vaisyas_non_muslim_influence_modifier = 0.1

		# ANBENNAR
		castonath_patricians_influence_modifier = 0.1
		castonath_patricians_loyalty_modifier = 0.05
	}
}

loyal_nobility_reform = {
	icon = "nobleman"
	allow_normal_conversion = yes
	potential = {
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	modifiers = {
		army_tradition = 0.25
		nobles_influence_modifier = 0.1
		maratha_exclusive_influence_modifier = 0.1
		rajput_exclusive_influence_modifier = 0.1
		nobles_loyalty_modifier = 0.05
		maratha_exclusive_loyalty_modifier = 0.05
		rajput_exclusive_loyalty_modifier = 0.05
	}
}

monastic_breweries_reform = {
	potential = {
		OR = {
			religion = catholic
			culture_group = gaelic
			primary_culture = scottish
		}
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	trigger = {
		OR = {
			religion = catholic
			culture_group = gaelic
			primary_culture = scottish
		}
		any_owned_province = {
			OR = {
				trade_goods = wine
				trade_goods = grain
			}
		}
	}
	icon = "monastic_breweries_reform"
	allow_normal_conversion = yes
	modifiers = {
		global_trade_goods_size_modifier = 0.1
	}
	effect = {
		custom_tooltip = producing_wine_and_grain_tt
		hidden_effect = {
			every_owned_province = {
				if = {
					limit = {
						trade_goods = wine
					}
					add_province_modifier = { 
						name = wine_bonus_prod
						duration = -1
						hidden = yes
					}
				}
				if = {
					limit = {
						trade_goods = grain
					}
					add_province_modifier = { 
						name = grain_bonus_prod
						duration = -1
						hidden = yes
					}
				}
			}
		}
	}
	removed_effect = {
		hidden_effect = {
			every_owned_province = {
				limit = {
					OR = {
						has_province_modifier = wine_bonus_prod
						has_province_modifier = grain_bonus_prod
					}
				}
				remove_province_modifier = wine_bonus_prod
				remove_province_modifier = grain_bonus_prod
			}
		}
	}
}

warrior_monks_reform = {
	potential = {
		OR = {
			religion_group = eastern
			AND = {
				OR = {
					culture_group = japanese_g
					primary_culture = ainu
				}
				capital_scope = {
					region = japan_region
				}
			}
			have_had_reform = warrior_monks_reform
		}
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	icon = "warrior_monks_reform"
	allow_normal_conversion = yes
	modifiers = {
		discipline = 0.05
		infantry_power = 0.05
		mercenary_manpower = 0.1
	}
}

guru_advisors_reform = {
	potential = {
		religion = sikhism
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	trigger = {
		religion = sikhism
	}
	icon = "nayankara_system"
	allow_normal_conversion = yes
	modifiers = {
		global_missionary_strength = 0.03
		global_institution_spread = 0.1
	}
}

#Clergy in Administration - Anbennar

local_administration = {
	icon = "judge"
	potential = {
		has_reform = adventurer_reform
		OR = {
			is_adventurer = yes
			is_playing_custom_nation = yes
		}
	}
	modifiers = {
		tribal_development_growth = 0.04 # Maybe tie feudal escanni formable to this?
		development_cost = -0.05
	}
	
	effect = {
		custom_tooltip = can_form_feudal_tt
	}
}

adventurer_administration = {
	icon = "government_for_people_reform"
	potential = {
		has_reform = adventurer_reform
		OR = {
			is_adventurer = yes
			is_playing_custom_nation = yes
		}
	}
	modifiers = {
		discipline = 0.05
		manpower_recovery_speed = 0.15
	}
}

leadership_of_the_clans = {
	icon = "soldiers_4"
	potential = {
		has_reform = dwarovar_adventurer_reform
	}
	modifiers = {
		stability_cost_modifier = -0.15
	}
}

cartel_oversight = {
	icon = "soldiers_4"
	potential = {
		has_reform = dwarovar_adventurer_reform
	}
	modifiers = {
		trade_efficiency = 0.1
	}
}


#Secularization

maintain_religious_head_reform = {
	icon = "nobleman"
	allow_normal_conversion = yes
	potential = {
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	modifiers = {
		yearly_absolutism = 1
	}
}

partial_secularisation_reform = {
	icon = "partial_secularisation_reform"
	allow_normal_conversion = yes
	potential = {
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	modifiers = {
		idea_cost = -0.1
		global_institution_spread = 0.05
	}
}

battle_pope_reform = {
	icon = "soldiers_6"
	allow_normal_conversion = yes
	potential = {
		has_reform = papacy_reform
	}

	rulers_can_be_generals = yes

	modifiers = {
		#manpower_in_true_faith_provinces = 0.10
		leader_land_fire = 3	#Anbennar
	}
	ai = {
		factor = 3
	}
}

conciliarism_reform = {
	icon = "conciliarism_reform"
	allow_normal_conversion = yes
	potential = {
		#tag = PAP
		tag = Z97
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	modifiers = {
		appoint_cardinal_cost = -0.3
	}
	ai = {
		factor = 3
	}
}

hereditary_religious_leadership_reform = {
	potential = {
		religion_group = muslim
	}
	icon = "muslim_highlighted"
	allow_normal_conversion = yes
	effect = {
		set_country_flag = populists_in_government
		change_government = monarchy
		add_government_reform = feudal_theocracy
	}
	ai = {
		factor = 0
	}
}

crown_leader_reform = {
	icon = "crown_highlighted"
	allow_normal_conversion = yes
	potential = {
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	trigger = {
		NOT = { has_government_attribute_short_desc = locked_government_type }
	}
	effect = {
		set_country_flag = populists_in_government
		lose_reforms = 3
		change_government = monarchy
	}
	ai = {
		factor = 0
	}
}

proclaim_republic_reform = {
	icon = "parliament_highlighted"
	allow_normal_conversion = yes
	potential = {
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	trigger = {
		NOT = { has_government_attribute_short_desc = locked_government_type }
	}
	effect = {
		set_country_flag = populists_in_government
		lose_reforms = 2
		change_government = republic
	}
	ai = {
		factor = 0
	}
}

#Anbennar
found_our_nation = {
	icon = "become_rev_republic_reform"
	potential = {
		has_reform = adventurer_reform
		OR = {
			is_adventurer = yes
			is_playing_custom_nation = yes
		}
	}
	native_mechanic = no
	trigger = {
		if = {
			limit = { NOT = { has_reform = found_our_nation } }
			is_at_war = no
			OR = {
				num_of_cities = 3
				treasury = 500
			}
		}
	}
	effect = {
		every_tribal_land_province = {
			limit = { NOT = { owner = { exists = yes } } }
			settle_province = ROOT
			if = {
				# Expel all orcs and goblins
				limit = { OR = { has_orcish_majority_trigger = yes has_goblin_majority_trigger = yes } }
				change_culture = ROOT
				change_religion = ROOT
				add_devastation = 10
			}
		}
		if = {
			limit = { NOT = { num_of_cities = 3 } }
			add_treasury = -500
		}
	}
	modifiers = {
		colonists = 1
	}
}

reclaimers_of_the_dwarovar = {
	icon = "soldiers_4"
	potential = {
		has_reform = dwarovar_adventurer_reform
	}
	modifiers = {
		core_decay_on_your_own = -0.2
	}
}

#Separation of Powers

clerical_commission_reform = {
	icon = "clergyman"
	allow_normal_conversion = yes
	modifiers = {
		global_unrest = -1
		diplomatic_upkeep = 1	
	}
	
	potential = {
		NOT = { 
			OR = {
				has_reform = monastic_order_reform
				tag = KOJ
			}
		}
		NOT = { has_adventurer_reform = yes }
	}
	
	trigger = {
		NOT = { has_reform = monastic_order_reform }
	}
	
	ai = {
		factor = 1
	}
}

divine_guidance_reform = {
	icon = "divine_guidance_reform"
	allow_normal_conversion = yes
	modifiers = {
		max_absolutism = 20
	}
	
	potential = {
		NOT = { has_reform = monastic_order_reform }
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	
	trigger = {
		NOT = { has_reform = monastic_order_reform }
	}
	
	ai = {
		factor = 1
	}
}

theocratic_democracy_reform = {
	icon = "assembly_hall"
	modifiers = {
		global_unrest = -1
	}
	
	potential = {
		NOT = { 
			OR = {
				has_reform = monastic_order_reform
				tag = KOJ
			}
		}
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	
	trigger = {
		NOT = { has_reform = monastic_order_reform }
		if = {
			limit = { has_dlc = "Common Sense" }
			OR = {
				NOT = { has_estate = estate_nobles }
				crown_stronger_than_estate = { estate = estate_nobles }
			}
		}
	}

	conditional = {
		allow = { has_dlc = "Common Sense" }
		has_parliament = yes
		custom_attributes = {
			blocked_call_diet = yes
		}
	}
	ai = {
		factor = 1
		modifier = {
			factor = 0.5
			NOT = { technology_group = tech_cannorian }
			NOT = { technology_group = tech_gerudian }
		}
		modifier = {
			factor = 0.5
			NOT = { technology_group = tech_cannorian }
			NOT = { technology_group = tech_gerudian }
			NOT = {
				any_neighbor_country = {
					OR = {
						technology_group = tech_cannorian
						technology_group = tech_gerudian
					}
				}
			}
		}
		modifier = {
			factor = 0
			OR = {
				has_reform = mughal_government
				has_reform = jadd_nobility			#mughal equiv
				has_reform = celestial_empire
				#religion = confucianism
				religion_group = faithless
			}
		}
	}
}

regionally_elected_commanders = {
	icon = "regionally_elected_commanders"
	allow_normal_conversion = yes
	fixed_rank = 0
	modifiers = {
		global_unrest = -1
	}
	
	potential = {
		OR = {
			has_reform = monastic_order_reform
			tag = KOJ
		}
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	
	trigger = {
		OR = {
			has_reform = monastic_order_reform
			tag = KOJ
		}
		if = {
			limit = { has_dlc = "Common Sense" }
			OR = {
				NOT = { has_estate = estate_nobles }
				crown_stronger_than_estate = { estate = estate_nobles }
			}
		}
	}

	conditional = {
		allow = { has_dlc = "Common Sense" }
		has_parliament = yes
		custom_attributes = {
			blocked_call_diet = yes
		}
	}
	ai = {
		factor = 1
		modifier = {
			factor = 0.5
			NOT = { technology_group = western }
		}
		modifier = {
			factor = 0.5
			NOT = { technology_group = western }
			NOT = {
				any_neighbor_country = {
					technology_group = western
				}
			}
		}
		modifier = {
			factor = 0
			OR = {
				has_reform = mughal_government
				has_reform = celestial_empire
				religion = confucianism
			}
		}
	}
}

monastic_elections_reform = {
	icon = "monastic_elections_reform"
	allow_normal_conversion = yes
	fixed_rank = 0
	modifiers = {
		stability_cost_modifier = -0.1
		max_absolutism = 20
	}
	
	potential = {
		OR = {
			has_reform = monastic_order_reform
			tag = KOJ
		}
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	
	trigger = {
		OR = {
			has_reform = monastic_order_reform
			tag = KOJ
		}
	}
	
	conditional = {
		allow = { has_dlc = "Res Publica" }
		heir = no
		election_on_death = yes
		states_general_mechanic = {
			militarists = {
				manpower_recovery_speed = 0.2
				warscore_cost_vs_other_religion = -0.15
				army_tradition_from_battle = 0.5
			}

			theocrats = {
				tolerance_own = 2
				global_missionary_strength = 0.02
				devotion = 0.5
			}
		}
	}
	
	ai = {
		factor = 1
	}
}

commander_king_reform = {
	icon = "commander_king_reform"
	allow_normal_conversion = yes
	fixed_rank = 0
	potential = {
		OR = {
			has_reform = monastic_order_reform
			tag = KOJ
		}
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	trigger = {
		OR = {
			has_reform = monastic_order_reform
			tag = KOJ
		}
		full_idea_group = theocracy_gov_ideas
	}
	modifiers = {
		devotion = 0.1
		harsh_treatment_cost = -0.2
		leader_land_fire = 1
	}
	custom_attributes = {
		generals_become_rulers = yes
	}
	has_term_election = no
	queen = yes
	royal_marriage = yes
	heir = no
	ai = {
		factor = 0.1
	}
}

admiral_king_reform = {
	icon = "admiral_king_reform"
	allow_normal_conversion = yes
	fixed_rank = 0
	potential = {
		OR = {
			has_reform = monastic_order_reform
			tag = KOJ
		}
		capital_scope = {
			has_port = yes
		}
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	trigger = {
		OR = {
			has_reform = monastic_order_reform
			tag = KOJ
		}
		has_reform = mission_to_kill_pirates_reform
		OR = {
			full_idea_group = maritime_ideas
			full_idea_group = naval_ideas
		}
	}
	modifiers = {
		devotion = 0.1
		naval_morale = 0.15
		leader_naval_fire = 1
	}
	custom_attributes = {
		admirals_become_rulers = yes
	}
	has_term_election = no
	queen = yes
	royal_marriage = yes
	heir = no
	ai = {
		factor = 0.1
	}
}

#The Nature of Our Faith

church_and_state_reform = {
	potential = {
		NOT = {
			religion_group = pagan
		}
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	icon = "church_and_state_reform"
	allow_normal_conversion = yes
	modifiers = {
		free_policy = 1
	}
	ai = {
		factor = 0
	}
}

god_and_man_reform = {
	potential = {
		NOT = {
			religion_group = pagan
		}
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	icon = "religious_leader"
	allow_normal_conversion = yes
	modifiers = {
		missionaries = 1
		global_missionary_strength = 0.01
	}
	ai = {
		factor = 0
	}
}

religious_harmony_reform = {
	icon = "religious_harmony_reform"
	allow_normal_conversion = yes
	potential = {
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	trigger = {
		OR = {
			religion_group = pagan
			has_idea_group = humanist_ideas
		}
	}
	modifiers = {
		num_accepted_cultures = 2
	}
	ai = {
		factor = 0
	}
}

organising_our_religion_reform = {
	icon = "organising_our_religion_reform"
	allow_normal_conversion = yes
	potential = {
		religion_group = pagan
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	trigger = {
		religion_group = pagan
	}
	
	modifiers = {
		yearly_absolutism = 1
		devotion = 1
	}
	ai = {
		factor = 3
	}
}

#Global Religion

religion_in_one_country_reform = {
	icon = "rioting_burning_2"
	allow_normal_conversion = yes
	potential = {
		NOT = {
			religion_group = pagan
		}
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	trigger = {
		NOT = {
			religion_group = pagan
		}
	}
	modifiers = {
		warscore_cost_vs_other_religion = -0.3
	}
	ai = {
		factor = 3
	}
}

religious_permanent_revolution_reform = {
	icon = "religious_permanent_revolution_reform"
	allow_normal_conversion = yes
	force_conversion_gives_global_holy_war_released_modifier = yes
	potential = {
		NOT = {
			religion = confucianism
			religion_group = pagan
		}
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	trigger = {
		NOT = {
			religion = confucianism
			religion_group = pagan
		}
	}
	effect = {
		custom_tooltip = world_crusade_tt
	}
	modifiers = {
		enforce_religion_cost = -0.25
	}
	ai = {
		factor = 3
	}
}

many_fingers_of_god_reform = {
	icon = "asian_scripture"
	allow_normal_conversion = yes
	potential = {
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	trigger = {
		has_idea_group = humanist_ideas
	}
	modifiers = {
		tolerance_heretic = 2	
		tolerance_heathen = 2
	}
	ai = {
		factor = 3
	}
}

priestly_autonomy_reform = {
	icon = "strength_of_the_khalsa"
	allow_normal_conversion = yes
	potential = {
		religion_group = pagan
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	trigger = {
		religion_group = pagan
	}
	
	modifiers = {
		governing_capacity = 250
	}
	ai = {
		factor = 3
	}
}

proclaim_religious_head_reform = {
	icon = "shaman"
	allow_normal_conversion = yes
	potential = {
		religion_group = pagan
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	trigger = {
		religion_group = pagan
		custom_trigger_tooltip = {
			tooltip = no_other_head_tt
			NOT = {
				any_country = {
					has_reform = proclaim_religious_head_reform
					religion = ROOT
					tag = ROOT
				}
			}
		}
	}
	modifiers = {
		warscore_cost_vs_other_religion = -0.25
		diplomatic_reputation = 1
	}
	ai = {
		factor = 3
	}
}

all_under_tengri_reform = {
	icon = "all_under_tengri_reform"
	allow_normal_conversion = yes
	potential = {
		religion = tengri_pagan_reformed
		NOT = { has_adventurer_reform = yes } # Anbennar
	}
	trigger = {
		religion = tengri_pagan_reformed
	}
	modifiers = {
		cavalry_power = 0.15
		cav_to_inf_ratio = 0.25
	}
	ai = {
		factor = 3
	}
}

sacred_elven_empire_reform = {  #Dahvar Aelnar government
	icon = "sacred_elven_empire"
	allow_normal_conversion = no
	potential = {
		has_country_flag = aelnar_dahvar_government
		OR = {
			tag = Z43
		}
	}
	valid_for_nation_designer = yes
	nation_designer_cost = 10
	nation_designer_trigger = {
	
	}
	lock_level_when_selected = yes
	fixed_rank = 3
	claim_states = yes
	
	modifiers = {
		promote_culture_cost = 3
		governing_capacity_modifier = 0.1
		tolerance_own = 2
		global_missionary_strength = 0.01
		missionary_maintenance_cost = 0.15
		church_loyalty_modifier = 0.05
		max_absolutism = 10
		missionaries = 1
	}
	custom_attributes = {
		locked_government_type = yes
	}
}

beatific_birth_reform = {
	icon = "beatific_birth"
	allow_normal_conversion = yes
	potential = {
		has_country_flag = aelnar_ciment_the_new_administration
	}
	
	trigger = {
		religion = nur_dahvaraesa
	}
	
	modifiers = {
		manpower_recovery_speed = 0.25
		global_trade_goods_size_modifier = -0.1
	}
	ai = {
		factor = 10
	}
}

sacred_body_reform = {
	icon = "strength_of_the_khalsa"
	allow_normal_conversion = yes
	potential = {
		has_country_flag = aelnar_ciment_the_new_administration
	}
	
	trigger = {
		religion = nur_dahvaraesa
	}
	
	modifiers = {
		shock_damage_received = -0.03
		fire_damage_received = -0.03
	}
	ai = {
		factor = 10
	}
}

administration_in_the_clergy_reform = {
	icon = "consolidate_power_in_doge_reform"
	allow_normal_conversion = yes
	potential = {
		has_country_flag = aelnar_ciment_the_new_administration
	}
	
	trigger = {
		religion = nur_dahvaraesa
	}
	
	modifiers = {
		yearly_corruption = 0.1
		governing_capacity_modifier = 0.20
	}
	ai = {
		factor = 10
	}
}

godhood_reform = {
	icon = "godhood"
	allow_normal_conversion = yes
	rulers_can_be_generals = yes
	potential = {
		has_country_flag = elven_constellation
	}
	
	trigger = {
		religion = nur_dahvaraesa
	}
	
	modifiers = {
		country_diplomatic_power = 1
		tolerance_own = 2
	}
	
	custom_attributes = {
		elven_constellation = yes
	}
	
	ai = {
		factor = 10
	}
}
