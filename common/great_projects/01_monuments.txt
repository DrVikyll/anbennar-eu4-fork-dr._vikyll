# duty = {
    # ambient_object = duty
    # province = 5253
    # is_canal = no
    # time = 120
    
    # # Applied to the province defined above on project completion
    # modifier = {
		# global_unrest = -1
    # }
# }

bone_citadel = {
	start = 1948

	date = 229.1.1

	time = {
		months = 120
	}

	build_cost = 1000

	can_be_moved = no

	starting_tier = 1

	#project type
	type = monument

	build_trigger = {
		OR = {		
			AND = {
				religion = death_cult_of_cheshosh
				has_owner_religion = yes
			}
			AND = {
				religion = regent_court
				has_owner_religion = yes
			}
		}
	}

	on_built = {
	}

	on_destroyed = {
	}

	can_use_modifiers_trigger = {
		OR = {		
			AND = {
				religion = death_cult_of_cheshosh
				has_owner_religion = yes
			}
			AND = {
				religion = regent_court
				has_personal_deity = nerat
				has_owner_religion = yes
			}
		}
	}

	can_upgrade_trigger = {
		OR = {		
			AND = {
				religion = death_cult_of_cheshosh
				has_owner_religion = yes
			}
			AND = {
				religion = regent_court
				has_owner_religion = yes
			}
		}
	}

	keep_trigger = {
	}

	#tier data
	tier_0 = {
		upgrade_time = {
			months = 0
		}

		cost_to_upgrade = {
			factor = 0
		}

		province_modifiers = {
		}

		area_modifier = {
		}

		country_modifiers = {

		}

		on_upgraded = {
		}
	}

	tier_1 = {
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 1000
		}

		province_modifiers = {
			
		}

		area_modifier = {
		}

		country_modifiers = {
			global_missionary_strength = 0.005
			tolerance_own = 0.5
			reduced_liberty_desire = 10
		}

		on_upgraded = {
			owner = {
				add_estate_loyalty = {
					estate = estate_church
					loyalty = 5
				}
			}
		}
	}

	tier_2 = {
		upgrade_time = {
			months = 120
		}

		
		cost_to_upgrade = {
			factor = 2500
		}

		
		province_modifiers = {
			
		}

		
		area_modifier = {
		}

		
		country_modifiers = {
			global_missionary_strength = 0.01
			governing_capacity_modifier = 0.10
			tolerance_own = 1
			reduced_liberty_desire = 15
		}

		
		on_upgraded = {
			owner = {
				add_estate_loyalty = {
					estate = estate_church
					loyalty = 10
				}
			}
		}
	}

	tier_3 = {
		
		upgrade_time = {
			months = 120
		}

		
		cost_to_upgrade = {
			factor = 5000
		}

		
		province_modifiers = {
			
		}

		
		area_modifier = {
		}

		
		country_modifiers = {
			global_missionary_strength = 0.02
			governing_capacity_modifier = 0.25
			tolerance_own = 1
			reduced_liberty_desire = 15
		}
		
		on_upgraded = {
		}
	}
}

dobondotimveb = {
	start = 1044

	date = 245.1.1

	time = {
		months = 120
	}

	build_cost = 1000

	can_be_moved = no

	starting_tier = 1

	#project type
	type = monument

	build_trigger = {
		religion = totemism
		has_owner_religion = yes
	}

	on_built = {
	}

	on_destroyed = {
	}

	can_use_modifiers_trigger = {
		religion = totemism
		has_owner_religion = yes
	}

	can_upgrade_trigger = {
		religion = totemism
		has_owner_religion = yes
	}

	keep_trigger = {
	}

	#tier data
	tier_0 = {
		upgrade_time = {
			months = 0
		}

		cost_to_upgrade = {
			factor = 0
		}

		province_modifiers = {
		}

		area_modifier = {
		}

		country_modifiers = {

		}

		on_upgraded = {
		}
	}

	tier_1 = {
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 1000
		}

		province_modifiers = {
			
		}

		area_modifier = {
		}

		country_modifiers = {
			tolerance_heretic = 0.5
			tolerance_heathen = 0.5
		}

		on_upgraded = {
		}
	}

	tier_2 = {
		upgrade_time = {
			months = 120
		}

		
		cost_to_upgrade = {
			factor = 2500
		}

		
		province_modifiers = {
			
		}

		
		area_modifier = {
		}

		
		country_modifiers = {
			tolerance_heretic = 0.5
			tolerance_heathen = 0.5	
			stability_cost_modifier = -0.1
		}

		
		on_upgraded = {
		}
	}

	tier_3 = {
		
		upgrade_time = {
			months = 120
		}

		
		cost_to_upgrade = {
			factor = 5000
		}

		
		province_modifiers = {
			
		}

		
		area_modifier = {
		}

		
		country_modifiers = {
			tolerance_heretic = 1
			tolerance_heathen = 1						
			stability_cost_modifier = -0.25
			global_missionary_strength = 0.01
		}
		
		on_upgraded = {
		}
	}
}

bal_vertesk = {
	start = 216

	date = 1.1.1

	time = {
		months = 120
	}

	build_cost = 0

	can_be_moved = no

	starting_tier = 0

	#project type
	type = monument

	build_trigger = {
	}

	on_built = {
	}

	on_destroyed = {
	}

	can_use_modifiers_trigger = {
		NOT = { has_province_modifier = ruined_castanorian_citadel }
	}

	can_upgrade_trigger = {
		NOT = { has_province_modifier = ruined_castanorian_citadel }
	}

	keep_trigger = {
	}

	#tier data
	tier_0 = {
		upgrade_time = {
			months = 0
		}

		cost_to_upgrade = {
			factor = 0
		}

		province_modifiers = {
		}

		area_modifier = {
		}

		country_modifiers = {

		}

		on_upgraded = {
		}
	}

	tier_1 = {
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 1000
		}

		province_modifiers = {
			local_defensiveness = 0.05
		}

		area_modifier = {
		}

		country_modifiers = {
			prestige = 0.1
			spy_offence = 0.05
		}

		on_upgraded = {
			remove_province_modifier = A23_black_tower_renovations
			if = {
				limit = { owner = { has_estate = estate_castonath_patricians } }
				owner = {
					add_estate_loyalty = {
						estate = estate_castonath_patricians
						loyalty = 15
					}
				}
			}
		}
	}

	tier_2 = {
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 2500
		}

		province_modifiers = {
			local_defensiveness = 0.1
			local_unrest = -1
		}

		area_modifier = {
		}

		country_modifiers = {
			prestige = 0.25
			spy_offence = 0.05
			global_spy_defence = 0.10
		}

		on_upgraded = {
			remove_province_modifier = A23_black_tower_renovations
			if = {
				limit = { owner = { has_estate = estate_castonath_patricians } }
				owner = {
					add_estate_loyalty = {
						estate = estate_castonath_patricians
						loyalty = 15
					}
				}
			}
		}
	}

	tier_3 = {
		
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 5000
		}

		province_modifiers = {
			local_defensiveness = 0.10
		}

		area_modifier = {
			local_defensiveness = 0.10
		}

		country_modifiers = {
			prestige = 0.5
			global_unrest = -1 #maybe nerf this to local?
			spy_offence = 0.15
			global_spy_defence = 0.20
		}

		on_upgraded = {
			if = {
				limit = { owner = { has_estate = estate_castonath_patricians } }
				owner = {
					add_estate_loyalty = {
						estate = estate_castonath_patricians
						loyalty = 15
					}
				}
			}
			remove_province_modifier = A23_black_tower_renovations
		}
	}
}

bal_ouord = {
	start = 510

	date = 1.1.1

	time = {
		months = 120
	}

	build_cost = 0

	can_be_moved = no

	starting_tier = 0

	#project type
	type = monument

	build_trigger = {
	}

	on_built = {
	}

	on_destroyed = {
	}

	can_use_modifiers_trigger = {
		NOT = { has_province_modifier = ruined_castanorian_citadel }
	}

	can_upgrade_trigger = {
		NOT = { has_province_modifier = ruined_castanorian_citadel }
	}

	keep_trigger = {
	}

	#tier data
	tier_0 = {
		upgrade_time = {
			months = 0
		}

		cost_to_upgrade = {
			factor = 0
		}

		province_modifiers = {
		}

		area_modifier = {
		}

		country_modifiers = {

		}

		on_upgraded = {
		}
	}

	tier_1 = {
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 1000
		}

		province_modifiers = {
			local_defensiveness = 0.05
		}

		area_modifier = {
		}

		country_modifiers = {
			prestige = 0.1
			army_tradition = 0.25
			manpower_recovery_speed = 0.05
		}

		on_upgraded = {
			if = {
				limit = { owner = { has_estate = estate_castonath_patricians } }
				owner = {
					add_estate_loyalty = {
						estate = estate_castonath_patricians
						loyalty = 5
					}
				}
			}
		}
	}

	tier_2 = {
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 2500
		}

		province_modifiers = {
			local_defensiveness = 0.1
		}

		area_modifier = {
		}

		country_modifiers = {
			prestige = 0.25
			army_tradition = 0.25
			manpower_recovery_speed = 0.10
		}

		on_upgraded = {
			if = {
				limit = { owner = { has_estate = estate_castonath_patricians } }
				owner = {
					add_estate_loyalty = {
						estate = estate_castonath_patricians
						loyalty = 15
					}
				}
			}
		}
	}

	tier_3 = {
		
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 5000
		}

		province_modifiers = {
			local_defensiveness = 0.10
		}

		area_modifier = {
			local_defensiveness = 0.10
		}

		country_modifiers = {
			prestige = 0.5
			army_tradition = 0.5
			manpower_recovery_speed = 0.15
		}

		on_upgraded = {
			if = {
				limit = { owner = { has_estate = estate_castonath_patricians } }
				owner = {
					add_estate_loyalty = {
						estate = estate_castonath_patricians
						loyalty = 15
					}
				}
			}
		}
	}
}

bal_dostan = {
	start = 441

	date = 1.1.1

	time = {
		months = 120
	}

	build_cost = 0

	can_be_moved = no

	starting_tier = 0

	#project type
	type = monument

	build_trigger = {
	}

	on_built = {
	}

	on_destroyed = {
	}

	can_use_modifiers_trigger = {
		NOT = { has_province_modifier = ruined_castanorian_citadel }
	}

	can_upgrade_trigger = {
		NOT = { has_province_modifier = ruined_castanorian_citadel }
	}

	keep_trigger = {
	}

	#tier data
	tier_0 = {
		upgrade_time = {
			months = 0
		}

		cost_to_upgrade = {
			factor = 0
		}

		province_modifiers = {
		}

		area_modifier = {
		}

		country_modifiers = {

		}

		on_upgraded = {
		}
	}

	tier_1 = {
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 1000
		}

		province_modifiers = {
			local_defensiveness = 0.05
		}

		area_modifier = {
		}

		country_modifiers = {
			prestige = 0.1
			yearly_absolutism = 0.1
		}

		on_upgraded = {
			owner = {
				add_estate_loyalty = {
					estate = estate_nobles
					loyalty = 5
				}
				
				if = {
					limit = { has_estate = estate_castonath_patricians }
					add_estate_loyalty = {
						estate = estate_castonath_patricians
						loyalty = 15
					}
				}
				if = {
					limit = {
						has_estate = estate_vampires
					}
					add_estate_loyalty = {
						estate = estate_vampires
						loyalty = 5
					}
				}
			}
		}
	}

	tier_2 = {
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 2500
		}

		province_modifiers = {
			local_defensiveness = 0.1
		}

		area_modifier = {
		}

		country_modifiers = {
			prestige = 0.25
			yearly_absolutism = 0.25
		}

		on_upgraded = {
			owner = {
				add_estate_loyalty = {
					estate = estate_nobles
					loyalty = 10
				}
				if = {
					limit = {
						has_estate = estate_castonath_patricians
					}
					add_estate_loyalty = {
						estate = estate_castonath_patricians
						loyalty = 10
					}
				}
				if = {
					limit = {
						has_estate = estate_vampires
					}
					add_estate_loyalty = {
						estate = estate_vampires
						loyalty = 10
					}
				}
			}
		}
	}

	tier_3 = {
		
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 5000
		}

		province_modifiers = {
			local_defensiveness = 0.10
		}

		area_modifier = {
			local_defensiveness = 0.10
		}

		country_modifiers = {
			prestige = 0.5
			yearly_absolutism = 0.5
			nobles_loyalty_modifier = 0.05
			vampires_loyalty_modifier = 0.05
		}

		on_upgraded = {
			if = {
				limit = { owner = { has_estate = estate_castonath_patricians } }
				owner = {
					add_estate_loyalty = {
						estate = estate_castonath_patricians
						loyalty = 15
					}
				}
			}
		}
	}
}

bal_vroren = {
	start = 740

	date = 1.1.1

	time = {
		months = 120
	}

	build_cost = 0

	can_be_moved = no

	starting_tier = 0

	#project type
	type = monument

	build_trigger = {
	}

	on_built = {
	}

	on_destroyed = {
	}

	can_use_modifiers_trigger = {
		NOT = { has_province_modifier = ruined_castanorian_citadel }
	}

	can_upgrade_trigger = {
		NOT = { has_province_modifier = ruined_castanorian_citadel }
	}

	keep_trigger = {
	}

	#tier data
	tier_0 = {
		upgrade_time = {
			months = 0
		}

		cost_to_upgrade = {
			factor = 0
		}

		province_modifiers = {
		}

		area_modifier = {
		}

		country_modifiers = {

		}

		on_upgraded = {
		}
	}

	tier_1 = {
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 1000
		}

		province_modifiers = {
			local_defensiveness = 0.05
			garrison_size = 0.05 # Paradox pls fix
		}

		area_modifier = {
			supply_limit_modifier = 0.25
		}

		country_modifiers = {
			prestige = 0.1
			leader_cost = -0.05
		}

		on_upgraded = {
			if = {
				limit = { owner = { has_estate = estate_castonath_patricians } }
				owner = {
					add_estate_loyalty = {
						estate = estate_castonath_patricians
						loyalty = 5
					}
				}
			}
		}
	}

	tier_2 = {
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 2500
		}

		province_modifiers = {
			local_defensiveness = 0.1
			garrison_size = 0.10 # Paradox pls fix
		}

		area_modifier = {
			supply_limit_modifier = 0.33
		}

		country_modifiers = {
			prestige = 0.25
			leader_cost = -0.075
		}

		on_upgraded = {
			if = {
				limit = { owner = { has_estate = estate_castonath_patricians } }
				owner = {
					add_estate_loyalty = {
						estate = estate_castonath_patricians
						loyalty = 10
					}
				}
			}
		}
	}

	tier_3 = {
		
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 5000
		}

		province_modifiers = {
			local_defensiveness = 0.10
			garrison_size = 0.15 # Paradox pls fix
		}

		area_modifier = {
			local_defensiveness = 0.10
			supply_limit_modifier = 0.50
		}

		country_modifiers = {
			prestige = 0.5
			leader_cost = -0.10
		}

		on_upgraded = {
			if = {
				limit = { owner = { has_estate = estate_castonath_patricians } }
				owner = {
					add_estate_loyalty = {
						estate = estate_castonath_patricians
						loyalty = 15
					}
				}
			}
		}
	}
}

bal_hyl = {
	start = 306

	date = 1.1.1

	time = {
		months = 120
	}

	build_cost = 0

	can_be_moved = no

	starting_tier = 0

	#project type
	type = monument

	build_trigger = {
	}

	on_built = {
	}

	on_destroyed = {
	}

	can_use_modifiers_trigger = {
		NOT = { has_province_modifier = ruined_castanorian_citadel }
	}

	can_upgrade_trigger = {
		NOT = { has_province_modifier = ruined_castanorian_citadel }
	}

	keep_trigger = {
	}

	#tier data
	tier_0 = {
		upgrade_time = {
			months = 0
		}

		cost_to_upgrade = {
			factor = 0
		}

		province_modifiers = {
		}

		area_modifier = {
		}

		country_modifiers = {

		}

		on_upgraded = {
		}
	}

	tier_1 = {
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 1000
		}

		province_modifiers = {
			local_defensiveness = 0.05
			hostile_attrition = 1
		}

		area_modifier = {
		}

		country_modifiers = {
			prestige = 0.1
		}

		on_upgraded = {
			if = {
				limit = { owner = { has_estate = estate_castonath_patricians } }
				owner = {
					add_estate_loyalty = {
						estate = estate_castonath_patricians
						loyalty = 5
					}
				}
			}
		}
	}

	tier_2 = {
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 2500
		}

		province_modifiers = {
			local_defensiveness = 0.1
			trade_goods_size = 0.25
		}

		area_modifier = {
			hostile_attrition = 1
		}

		country_modifiers = {
			prestige = 0.25
		}

		on_upgraded = {
			if = {
				limit = { owner = { has_estate = estate_castonath_patricians } }
				owner = {
					add_estate_loyalty = {
						estate = estate_castonath_patricians
						loyalty = 10
					}
				}
			}
		}
	}

	tier_3 = {
		
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 5000
		}

		province_modifiers = {
			local_defensiveness = 0.10
			trade_goods_size = 0.5
		}

		area_modifier = {
			local_defensiveness = 0.10
			hostile_attrition = 2
		}

		country_modifiers = {
			prestige = 0.5
		}

		on_upgraded = {
			if = {
				limit = { owner = { has_estate = estate_castonath_patricians } }
				owner = {
					add_estate_loyalty = {
						estate = estate_castonath_patricians
						loyalty = 15
					}
				}
			}
		}
	}
}

bal_mire = {
	start = 229

	date = 1.1.1

	time = {
		months = 120
	}

	build_cost = 0

	can_be_moved = no

	starting_tier = 0

	#project type
	type = monument

	build_trigger = {
	}

	on_built = {
	}

	on_destroyed = {
	}

	can_use_modifiers_trigger = {
		NOT = { has_province_modifier = ruined_castanorian_citadel }
	}

	can_upgrade_trigger = {
		NOT = { has_province_modifier = ruined_castanorian_citadel }
	}

	keep_trigger = {
	}

	#tier data
	tier_0 = {
		upgrade_time = {
			months = 0
		}

		cost_to_upgrade = {
			factor = 0
		}

		province_modifiers = {
		}

		area_modifier = {
		}

		country_modifiers = {

		}

		on_upgraded = {
		}
	}

	tier_1 = {
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 1000
		}

		province_modifiers = {
			local_defensiveness = 0.05
			local_manpower_modifier = 0.15
		}

		area_modifier = {
			local_autonomy = -0.01
		}

		country_modifiers = {
			prestige = 0.1
			rival_border_fort_maintenance = -0.10
		}

		on_upgraded = {
			if = {
				limit = { owner = { has_estate = estate_castonath_patricians } }
				owner = {
					add_estate_loyalty = {
						estate = estate_castonath_patricians
						loyalty = 5
					}
				}
			}
		}
	}

	tier_2 = {
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 2500
		}

		province_modifiers = {
			local_defensiveness = 0.1
			local_manpower_modifier = 0.25
		}

		area_modifier = {
			local_autonomy = -0.025
		}

		country_modifiers = {
			prestige = 0.25
			rival_border_fort_maintenance = -0.15
		}

		on_upgraded = {
			if = {
				limit = { owner = { has_estate = estate_castonath_patricians } }
				owner = {
					add_estate_loyalty = {
						estate = estate_castonath_patricians
						loyalty = 10
					}
				}
			}
		}
	}

	tier_3 = {
		
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 5000
		}

		province_modifiers = {
			local_defensiveness = 0.10
			local_manpower_modifier = 0.50
		}

		area_modifier = {
			local_defensiveness = 0.10
			local_autonomy = -0.05
		}

		country_modifiers = {
			prestige = 0.5
			rival_border_fort_maintenance = -0.25
		}

		on_upgraded = {
			if = {
				limit = { owner = { has_estate = estate_castonath_patricians } }
				owner = {
					add_estate_loyalty = {
						estate = estate_castonath_patricians
						loyalty = 15
					}
				}
			}
		}
	}
}

the_north_citadel = {
	start = 840

	date = 1.1.1

	time = {
		months = 120
	}

	build_cost = 0

	can_be_moved = no

	starting_tier = 0

	#project type
	type = monument

	build_trigger = {
	}

	on_built = {
	}

	on_destroyed = {
	}

	can_use_modifiers_trigger = {
		NOT = { has_province_modifier = ruined_castanorian_citadel }
	}

	can_upgrade_trigger = {
		NOT = { has_province_modifier = ruined_castanorian_citadel }
	}

	keep_trigger = {
	}

	#tier data
	tier_0 = {
		upgrade_time = {
			months = 0
		}

		cost_to_upgrade = {
			factor = 0
		}

		province_modifiers = {
		}

		area_modifier = {
		}

		country_modifiers = {

		}

		on_upgraded = {
		}
	}

	tier_1 = {
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 1000
		}

		province_modifiers = {
			local_defensiveness = 0.05
		}

		area_modifier = {
		}

		country_modifiers = {
			prestige = 0.1
			legitimacy = 0.25
			governing_capacity_modifier = 0.05
		}

		on_upgraded = {
			owner = {
				add_estate_loyalty = {
					estate = estate_castonath_patricians
					loyalty = 5
				}
			}
		}
	}

	tier_2 = {
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 2500
		}

		province_modifiers = {
			local_defensiveness = 0.1
		}

		area_modifier = {
		}

		country_modifiers = {
			prestige = 0.25
			legitimacy = 0.5
			governing_capacity_modifier = 0.10
			vassal_forcelimit_bonus = 0.25
		}

		on_upgraded = {
			owner = {
				add_estate_loyalty = {
					estate = estate_castonath_patricians
					loyalty = 10
				}
			}
		}
	}

	tier_3 = {
		
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 5000
		}

		province_modifiers = {
			local_defensiveness = 0.10
		}

		area_modifier = {
			local_defensiveness = 0.10
		}

		country_modifiers = {
			prestige = 0.5
			legitimacy = 1
			governing_capacity_modifier = 0.15
			castonath_patricians_loyalty_modifier = 0.05
			vassal_forcelimit_bonus = 0.5
		}

		on_upgraded = {
			owner = {
				add_estate_loyalty = {
					estate = estate_castonath_patricians
					loyalty = 15
				}
			}
		}
	}
}

aelcandar = {
	start = 162

	date = 1000.1.1

	time = {
		months = 120
	}

	build_cost = 0

	can_be_moved = no

	starting_tier = 0

	#project type
	type = monument

	build_trigger = {
	}

	on_built = {
	}

	on_destroyed = {
	}

	can_use_modifiers_trigger = {
	}

	can_upgrade_trigger = {
	}

	keep_trigger = {
	}

	#tier data
	tier_0 = {
		upgrade_time = {
			months = 0
		}

		cost_to_upgrade = {
			factor = 0
		}

		province_modifiers = {
		}

		area_modifier = {
		}

		country_modifiers = {

		}

		on_upgraded = {
		}
	}

	tier_1 = {
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 1000
		}

		province_modifiers = {
			local_defensiveness = 0.05
		}

		area_modifier = {
		}

		country_modifiers = {
		    prestige = 0.1
		}

		on_upgraded = {
		}
	}

	tier_2 = {
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 2500
		}

		province_modifiers = {
			local_defensiveness = 0.1
		}

		area_modifier = {
		}

		country_modifiers = {
		    prestige = 0.25
		}

		on_upgraded = {
		}
	}

	tier_3 = {
		
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 5000
		}

		province_modifiers = {
			local_defensiveness = 0.10
		}

		area_modifier = {
			local_defensiveness = 0.10
		}

		country_modifiers = {
		    prestige = 0.5
		}

		on_upgraded = {
		}
	}
}

escandar = {
	start = 316

	date = 1000.1.1

	time = {
		months = 120
	}

	build_cost = 0

	can_be_moved = no

	starting_tier = 0

	#project type
	type = monument

	build_trigger = {
	}

	on_built = {
	}

	on_destroyed = {
	}

	can_use_modifiers_trigger = {
	}

	can_upgrade_trigger = {
	}

	keep_trigger = {
	}

	#tier data
	tier_0 = {
		upgrade_time = {
			months = 0
		}

		cost_to_upgrade = {
			factor = 0
		}

		province_modifiers = {
		}

		area_modifier = {
		}

		country_modifiers = {

		}

		on_upgraded = {
		}
	}

	tier_1 = {
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 1000
		}

		province_modifiers = {
			local_defensiveness = 0.05
		}

		area_modifier = {
		}

		country_modifiers = {
		    prestige = 0.1
		}

		on_upgraded = {
		}
	}

	tier_2 = {
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 2500
		}

		province_modifiers = {
			local_defensiveness = 0.1
		}

		area_modifier = {
		}

		country_modifiers = {
		    prestige = 0.25
		}

		on_upgraded = {
		}
	}

	tier_3 = {
		
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 5000
		}

		province_modifiers = {
			local_defensiveness = 0.10
		}

		area_modifier = {
			local_defensiveness = 0.10
		}

		country_modifiers = {
		    prestige = 0.5
		}

		on_upgraded = {
		}
	}
}

calascandar = {
	start = 254

	date = 1000.1.1

	time = {
		months = 120
	}

	build_cost = 0

	can_be_moved = no

	starting_tier = 0

	#project type
	type = monument

	build_trigger = {
	}

	on_built = {
	}

	on_destroyed = {
	}

	can_use_modifiers_trigger = {
	}

	can_upgrade_trigger = {
	}

	keep_trigger = {
	}

	#tier data
	tier_0 = {
		upgrade_time = {
			months = 0
		}

		cost_to_upgrade = {
			factor = 0
		}

		province_modifiers = {
		}

		area_modifier = {
		}

		country_modifiers = {

		}

		on_upgraded = {
		}
	}

	tier_1 = {
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 1000
		}

		province_modifiers = {
			local_defensiveness = 0.05
		}

		area_modifier = {
		}

		country_modifiers = {
		    prestige = 0.1
		}

		on_upgraded = {
		}
	}

	tier_2 = {
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 2500
		}

		province_modifiers = {
			local_defensiveness = 0.1
		}

		area_modifier = {
		}

		country_modifiers = {
		    prestige = 0.25
		}

		on_upgraded = {
		}
	}

	tier_3 = {
		
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 5000
		}

		province_modifiers = {
			local_defensiveness = 0.10
		}

		area_modifier = {
			local_defensiveness = 0.10
		}

		country_modifiers = {
		    prestige = 0.5
		}

		on_upgraded = {
		}
	}
}

the_south_citadel = {
	start = 834

	date = 2021.1.1

	time = { months = 120 }

	build_cost = 0

	can_be_moved = no

	starting_tier = 0

	#project type
	type = monument

	build_trigger = {
		owner = { has_country_flag = castanor_south_citadel_restored }
	}

	on_built = {
	}

	on_destroyed = {
	}

	can_use_modifiers_trigger = {
	}

	can_upgrade_trigger = {
	}

	keep_trigger = {
	}

	#tier data
	tier_0 = {
		upgrade_time = {
			months = 0
		}

		cost_to_upgrade = {
			factor = 0
		}

		province_modifiers = {
		}

		area_modifier = {
		}

		country_modifiers = {

		}

		on_upgraded = {
		}
	}

	tier_1 = {
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 1000
		}

		province_modifiers = {
			local_defensiveness = 0.05
		}

		area_modifier = {
		}

		country_modifiers = {
			prestige = 0.1
			diplomatic_reputation = 0.5
		}

		on_upgraded = {
			owner = {
				add_estate_loyalty = {
					estate = estate_castonath_patricians
					loyalty = 5
				}
			}
		}
	}

	tier_2 = {
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 2500
		}

		province_modifiers = {
			local_defensiveness = 0.1
		}

		area_modifier = {
		}

		country_modifiers = {
			prestige = 0.25
			diplomatic_reputation = 0.5
			improve_relation_modifier = 0.05
		}

		on_upgraded = {
			owner = {
				add_estate_loyalty = {
					estate = estate_castonath_patricians
					loyalty = 10
				}
			}
		}
	}

	tier_3 = {
		
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 5000
		}

		province_modifiers = {
			local_defensiveness = 0.10
		}

		area_modifier = {
			local_defensiveness = 0.10
		}

		country_modifiers = {
			prestige = 0.5
			diplomatic_reputation = 1
			improve_relation_modifier = 0.1
		}

		on_upgraded = {
		}
	}
}

the_necropolis = {
	start = 332

	date = 1.1.1

	time = {
		months = 120
	}

	build_cost = 1000

	can_be_moved = no

	starting_tier = 1

	#project type
	type = monument

	build_trigger = {
		OR = {	
			AND = {
				religion_group = cannorian
				has_owner_religion = yes
			}
		}
	}

	on_built = {
	}

	on_destroyed = {
	}

	can_use_modifiers_trigger = {
		OR = {	
			AND = {
				religion_group = cannorian
				has_owner_religion = yes
			}
		}
	}

	can_upgrade_trigger = {
		OR = {	
			AND = {
				religion_group = cannorian
				has_owner_religion = yes
			}
		}
	}

	keep_trigger = {
	}

	#tier data
	tier_0 = {
		upgrade_time = {
			months = 0
		}

		cost_to_upgrade = {
			factor = 0
		}

		province_modifiers = {
		}

		area_modifier = {
		}

		country_modifiers = {

		}

		on_upgraded = {
		}
	}

	tier_1 = {
		upgrade_time = {
			months = 120
		}

		cost_to_upgrade = {
			factor = 1000
		}

		province_modifiers = {
		}

		area_modifier = {
			local_autonomy = -0.025
			local_tax_modifier = 0.05
		}

		country_modifiers = {
			global_missionary_strength = 0.01
			stability_cost_modifier = -0.05
		}

		on_upgraded = {
			owner = {
				add_estate_loyalty = {
					estate = estate_church
					loyalty = 5
				}
			}
		}
	}

	tier_2 = {
		upgrade_time = {
			months = 120
		}

		
		cost_to_upgrade = {
			factor = 2500
		}

		
		province_modifiers = {
			
		}

		
		area_modifier = {
			local_tax_modifier = 0.10
		}

		
		country_modifiers = {
			missionaries = 1
			global_missionary_strength = 0.02
			stability_cost_modifier = -0.10
			global_autonomy = -0.025
		}

		
		on_upgraded = {
			owner = {
				add_estate_loyalty = {
					estate = estate_church
					loyalty = 10
				}
			}
		}
	}

	tier_3 = {
		
		upgrade_time = {
			months = 120
		}

		
		cost_to_upgrade = {
			factor = 5000
		}

		
		province_modifiers = {
			
		}

		
		area_modifier = {
			local_tax_modifier = 0.15
		}

		
		country_modifiers = {
			missionaries = 1
			global_missionary_strength = 0.03
			stability_cost_modifier = -0.15
			global_autonomy = -0.05
		}
		
		on_upgraded = {
		}
	}
}